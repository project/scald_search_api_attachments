<?php

/**
 * @file
 * Search API data alteration callback.
 */
class ScaldSearchApiAttachmentsAlterSettings extends SearchApiAttachmentsAlterSettings {

  /**
   * {@inheritdoc}
   */
  public function alterItems(array &$items) {
    $exclude = array();
    foreach (explode(' ', $this->options['excluded_extensions']) as $ext) {
      $exclude[$ext] = file_get_mimetype('dummy.' . $ext);
    }

    // Check all fields if we have "Atom Reference" in current entity.
    $fields = field_info_fields();
    foreach ($items as $id => &$item) {
      foreach ($item as $field_name => $field) {
        if (!empty($fields[$field_name])) {
          $field = $fields[$field_name];
          if ($field['type'] == 'atom_reference') {

            // Catch all sids of referenced Scald Atom entities.
            $values = field_get_items($this->index->item_type, $item, $field_name);
            if (!empty($values)) {
              $ids = array();
              foreach ($values as $value) {
                $ids[] = $value['sid'];
              }
              // Load Scald Atom entities and check if thay have any files.
              if (!empty($ids)) {
                $scald_atoms = scald_atom_load_multiple($ids);
                foreach ($scald_atoms as $scald_atom) {
                  foreach ($scald_atom as $field_name => $field) {
                    if (!empty($fields[$field_name])) {
                      $field = $fields[$field_name];
                      if ($field['type'] == 'file') {
            
                        // Index the files content.
                        $values = field_get_items('scald_atom', $scald_atom, $field_name);
                        if (!empty($values)) {
                          foreach ($values as $file) {
            
                            // Skip files which need exclude.
                            if (!in_array($file['filemime'], $exclude) && !$this->is_temporary($file)) {
                              $attachments = 'attachments_' . $field_name;
                              if (isset($item->{$attachments})) {
                                $item->{$attachments} .= ' ' . $this->getFileContent($file);
                              }
                              else {
                                $item->{$attachments} = $this->getFileContent($file);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function propertyInfo() {
    $ret = array();
    if ($this->index->item_type == 'file') {
      $ret['attachments_content'] = array(
        'label' => 'File content',
        'description' => 'File content',
        'type' => 'text',
      );
    }
    else {
      $fields = $this->getFileFields();
      foreach ($fields as $name => $field) {
        $ret['attachments_' . $name] = array(
          'label' => 'Attachment content: ' . $name,
          'description' => $name,
          'type' => 'text',
        );
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileFields() {
    $ret = array();
    foreach (field_info_fields() as $name => $field) {
      if ($field['type'] == 'file') {
        $ret[$name] = $field;
      }
    }
    return $ret;
  }
}
